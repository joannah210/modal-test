import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-team-bgreet-action-modal',
  templateUrl: './team-bgreet-action-modal.component.html',
  styleUrls: ['./team-bgreet-action-modal.component.css']
})
export class TeamBGreetActionModalComponent implements OnInit {
  @Input() title: string = '';
  @Input() message: string = '';
  @Input() action: (() => string) | undefined;

  constructor() { }

  ngOnInit(): void {
  }

}
