import {Component} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {MessageModalComponent} from "./components/modal/message-modal/message-modal.component";
import {ActionService} from "./action.service";
import {TeamAHelloActionModalComponent} from "./team-a/team-ahello-action-modal/team-ahello-action-modal.component";
import {TeamBGreetActionModalComponent} from "./team-b/team-bgreet-action-modal/team-bgreet-action-modal.component";
import {TeamBPersonalGreetFormActionModalComponent} from "./team-b/team-bpersonal-greet-form-action-modal/team-bpersonal-greet-form-action-modal.component";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  actionResult: string = "no action yet";
  title = 'modal-test';

  constructor(private modalService: NgbModal, private actionService: ActionService) {
  }

  open(): void {
    const modalRef = this.modalService.open(MessageModalComponent);
    modalRef.componentInstance.title = "This is a title :)";
    modalRef.componentInstance.message = "Is it friday already?";
  }

  openActionOne(): void {
    const modalRef = this.modalService.open(TeamAHelloActionModalComponent);
    modalRef.componentInstance.title = "Say Hello";
    modalRef.componentInstance.message = "Nice to meet you :)";
    modalRef.componentInstance.action = this.actionService.greet;
    modalRef.result.then(x => this.actionResult = x);
  }

  openActionTwo(): void {
    const modalRef = this.modalService.open(TeamBGreetActionModalComponent);
    modalRef.componentInstance.title = "Say Goodbye";
    modalRef.componentInstance.message = "It was nice meeting you :)";
    modalRef.componentInstance.action = this.actionService.goodBye;
    modalRef.result.then(x => this.actionResult = x);
  }

  openCombinedAction(): void {
    const modalRef = this.modalService.open(TeamBGreetActionModalComponent);
    modalRef.componentInstance.title = "Hello ... Goodbye";
    modalRef.componentInstance.message = "Do you like the beetles?";
    modalRef.componentInstance.action = () => {
      return this.actionService.greet() + ' ... ' + this.actionService.goodBye();
    };
    modalRef.result.then(x => this.actionResult = x);
  }

  openPersonalAction(): void {
    const modalRef = this.modalService.open(TeamBPersonalGreetFormActionModalComponent);
    modalRef.componentInstance.title = "Who is there?";
    modalRef.componentInstance.formAction = () => {
      return this.actionService.greet() + ' ' +
      this.actionService.showFirstname(modalRef.componentInstance.formActionInput);
    };
    modalRef.result.then(x => this.actionResult = x);
  }
}
