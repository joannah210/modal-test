import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from './components/modal/modal.component';
import {ActionService} from "./action.service";
import { ModalBodyComponent } from './components/modal/modal-body/modal-body.component';
import { MessageModalComponent } from './components/modal/message-modal/message-modal.component';
import { ModalFooterComponent } from './components/modal/modal-footer/modal-footer.component';
import { ActionModalComponent } from './components/modal/action-modal/action-modal.component';
import {FormsModule} from "@angular/forms";
import { TeamAHelloActionModalComponent } from './team-a/team-ahello-action-modal/team-ahello-action-modal.component';
import { TeamBGreetActionModalComponent } from './team-b/team-bgreet-action-modal/team-bgreet-action-modal.component';
import { TeamBPersonalGreetFormActionModalComponent } from './team-b/team-bpersonal-greet-form-action-modal/team-bpersonal-greet-form-action-modal.component';

@NgModule({
  declarations: [
    AppComponent,
    ModalComponent,
    ModalBodyComponent,
    MessageModalComponent,
    ModalFooterComponent,
    ActionModalComponent,
    TeamAHelloActionModalComponent,
    TeamBGreetActionModalComponent,
    TeamBPersonalGreetFormActionModalComponent,
  ],
  imports: [
    BrowserModule,
    NgbModule,
    FormsModule
  ],
  providers: [ActionService],
  bootstrap: [AppComponent]
})
export class AppModule { }
