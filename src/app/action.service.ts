import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ActionService {

  constructor() { }

  greet() : string {
    return "Hello";
  }

  goodBye() : string {
    return "GoodBye!";
  }

  showFirstname(input: { firstname: string}) : string {
    return input.firstname;
  }
}
