import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamBPersonalGreetFormActionModalComponent } from './team-bpersonal-greet-form-action-modal.component';

describe('TeamBPersonalGreetFormActionModalComponent', () => {
  let component: TeamBPersonalGreetFormActionModalComponent;
  let fixture: ComponentFixture<TeamBPersonalGreetFormActionModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TeamBPersonalGreetFormActionModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamBPersonalGreetFormActionModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
