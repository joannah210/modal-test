import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-team-ahello-action-modal',
  templateUrl: './team-ahello-action-modal.component.html',
  styleUrls: ['./team-ahello-action-modal.component.css']
})
export class TeamAHelloActionModalComponent implements OnInit {
  @Input() title: string = '';
  @Input() message: string = '';
  @Input() action: (() => string) | undefined;

  constructor() { }

  ngOnInit(): void {
  }

}
