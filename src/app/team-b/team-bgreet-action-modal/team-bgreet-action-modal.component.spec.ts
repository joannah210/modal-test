import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamBGreetActionModalComponent } from './team-bgreet-action-modal.component';

describe('TeamBGreetActionModalComponent', () => {
  let component: TeamBGreetActionModalComponent;
  let fixture: ComponentFixture<TeamBGreetActionModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TeamBGreetActionModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamBGreetActionModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
