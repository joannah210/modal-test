import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-action-modal',
  templateUrl: './action-modal.component.html',
  styleUrls: ['./action-modal.component.css']
})
export class ActionModalComponent implements OnInit {
  @Input() title: string = "";
  @Input() action: (() => any) | undefined;

  constructor(public activeModal: NgbActiveModal) {}

  ngOnInit(): void {
  }

  performAction() : void {
    if(this.action !== undefined) {
      this.activeModal.close(this.action());
    }

    this.activeModal.close();
  }
}
