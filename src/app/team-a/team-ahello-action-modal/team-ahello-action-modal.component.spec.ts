import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamAHelloActionModalComponent } from './team-ahello-action-modal.component';

describe('TeamAHelloActionModalComponent', () => {
  let component: TeamAHelloActionModalComponent;
  let fixture: ComponentFixture<TeamAHelloActionModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TeamAHelloActionModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamAHelloActionModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
