import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-team-bpersonal-greet-form-action-modal',
  templateUrl: './team-bpersonal-greet-form-action-modal.component.html',
  styleUrls: ['./team-bpersonal-greet-form-action-modal.component.css']
})
export class TeamBPersonalGreetFormActionModalComponent implements OnInit {
  @Input() title: string = '';
  @Input() formAction: (() => string) | undefined;
  formActionInput: object = {};

  constructor() { }

  ngOnInit(): void {
  }

  updateFormInput(event: any) {
    this.formActionInput = {firstname: event.target.value};
  }

}
